variable "project_id" {}
variable "ssh_user" {}

variable "gcp_credentials_path" {}
variable "domain" {}

variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}

variable "instance_type" {
  default = "n1-standard-1"
}

