resource "google_compute_instance" "jenkins_instance" {
  name         = "jenkins-instance"
  machine_type = var.instance_type
  hostname     = "jenkins.${var.domain}"

  boot_disk {
    initialize_params {
      image = "almalinux-cloud/almalinux-8"
    }
  }

  labels = {
    environment = "testing"
  }

  # Esta instancia está configurada como Descartable (preemptible)
  # Dura 24 hrs maximo en ejecución. Google arbitrariamente la puede
  # terminar antes. Sale hasta 80% más ecomomico que una Instanca normal
  # Más info https://cloud.google.com/compute/docs/instances/preemptible
  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  network_interface {
    # A default network is created for all GCP projects
    network = google_compute_network.vpc_network.self_link
    access_config {
      nat_ip = google_compute_address.static.address
    }
  }

  metadata = {
    ssh-keys = "${var.ssh_user}:${file("~/.ssh/terraform-gcp.pub")}"
  }
}

resource "google_compute_address" "static" {
  name = "ipv4-address"
}
