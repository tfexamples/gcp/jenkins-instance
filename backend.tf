terraform {
  backend "gcs" {
    bucket      = "mapanare-net-tf-state-testing"
    prefix      = "terraform/state"
    credentials = "~/.gcp/mapanare-net-terraform-gke.json"
  }
}
