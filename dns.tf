
resource "google_dns_managed_zone" "domain-zone" {
  name        = "${var.domain}-zone"
  dns_name    = "${var.domain}."
  description = "${var.domain} Zone"
  labels = {
    environment = "testing"
  }
}
