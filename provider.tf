terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.84.0"
    }
  }
}

provider "google" {
  credentials = file(var.gcp_credentials_path)
  project     = var.project_id
  region      = var.region
  zone        = var.zone
}
